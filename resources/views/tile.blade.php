<x-dashboard-tile :position="$position">
    <div class="h-full ">
        <h1 class="font-medium text-dimmed text-sm text-center uppercase tracking-wide ">
            Current Incidents
        </h1>
        <div wire:poll.{{ $refreshIntervalInSeconds }}s class="divide-y-2">
            @foreach ($briefings as $briefing)
                <div class="flex flex-col py-2">
                    @if ($briefing['major_incident'])
                        <div class="pt-1 flex flex-row flex-grow">
                            <div class="flex-none text-4xl -mt-1 mr-2 text-gray-500 font-thin w-8 text-center">M</div>
                            <div class="">
                                <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Major Incident</div>
                                <div class="leading-tight">Declared</div>
                            </div>
                        </div>
                    @endif
                    <div class="pt-1 flex flex-row">
                        <div class="flex-none text-4xl -mt-1 mr-2 text-gray-500 font-thin w-8 text-center">E</div>
                        <div class="">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Exact Location</div>
                            <div class="leading-tight">{{$briefing['location']}}</div>
                        </div>
                    </div>
                    <div class="pt-1 flex flex-row">
                        <div class="flex-none text-4xl -mt-1 mr-2 text-gray-500 font-thin w-8 text-center">T</div>
                        <div class="">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Type of Incident</div>
                            <div class="leading-tight">{!!nl2br(e($briefing['type']))!!}</div>
                        </div>
                    </div>
                    <div class="pt-1 flex flex-row">
                        <div class="flex-none text-4xl -mt-1 mr-2 text-gray-500 font-thin w-8 text-center">H</div>
                        <div class="">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Hazards</div>
                            <div class="leading-tight">{!!nl2br(e($briefing['hazards']))!!}</div>
                        </div>
                    </div>
                    <div class="pt-1 flex flex-row">
                        <div class="flex-none text-4xl -mt-1 mr-2 text-gray-500 font-thin w-8 text-center">A</div>
                        <div class="">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Access and RV point</div>
                            <div class="leading-tight">{!!nl2br(e($briefing['access']))!!}</div>
                        </div>
                    </div>
                    <div class="pt-1 flex flex-row">
                        <div class="flex-none text-4xl -mt-1 mr-2 text-gray-500 font-thin w-8 text-center">N</div>
                        <div class="">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">No and type of casualties</div>
                            <div class="leading-tight">
                                {!!nl2br(e($briefing['casualty_text']))!!}
                            </div>
                        </div>
                    </div>
                    <div class="pt-1 flex flex-row">
                        <div class="flex-none text-4xl -mt-1 mr-2 text-gray-500 font-thin w-8 text-center">E</div>
                        <div class="">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Emergency Services/Responders required</div>
                            <div class="leading-tight">{!!nl2br(e($briefing['emergency_services']))!!}</div>
                        </div>
                    </div>
                    <div class="pt-1 mt-2">
                        <div class="ml-10">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Equipment required and notes</div>
                            <div class="leading-tight">{!!nl2br(e($briefing['notes']))!!}</div>
                        </div>
                    </div>
                    <div class="pt-1 mt-2">
                        <div class="ml-10">
                            <div class="font-medium text-gray-500 text-sm uppercase tracking-wide">Last Updated</div>
                            <div class="leading-tight">{{\Carbon\Carbon::create($briefing['updated_at'])->format('H:i D j M')}}</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-dashboard-tile>
