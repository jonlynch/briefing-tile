# A Tile to Show Briefing Information

[![Latest Version on Packagist](https://img.shields.io/packagist/v/jonlynch/briefing-tile.svg?style=flat-square)](https://packagist.org/packages/jonlynch/briefing-tile)
[![Total Downloads](https://img.shields.io/packagist/dt/jonlynch/briefing-tile.svg?style=flat-square)](https://packagist.org/packages/jonlynch/briefing-tile)

A tile to display briefing information

This tile can be used on [the Laravel Dashboard](https://docs.spatie.be/laravel-dashboard).

## Installation

You can install the package via composer:

```bash
$ composer require jonlynch/briefing-tile
```

## Usage

In your dashboard view you use the `livewire:briefing-tile` component. 

```html
<x-dashboard>
    <livewire:briefing-tile position="a1:a12" />
</x-dashboard>
```

Add the config to the tiles sections of your `config/dashboard.php`

```php
// in config/dashboard.php

return [
    // ...
    tiles => [
         'briefing' => [
            'url' => env('BRIEFING_URL'),
        ],
    ]
```

In `app\Console\Kernel.php` you should schedule the `JonLynch\BriefingTile\Commands\FetchBriefingDataCommand` to run every minute.

``` php
// in app\Console\Kernel.php

protected function schedule(Schedule $schedule)
{
    $schedule->command(\JonLynch\BriefingTile\Commands\FetchBriefingDataCommand::class)->everyMinute();

}
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email :author_email instead of using the issue tracker.

## Credits

- [Jon Lynch](https://github.com/jonlynch)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
