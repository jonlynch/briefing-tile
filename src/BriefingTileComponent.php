<?php

namespace JonLynch\BriefingTile;

use Livewire\Component;

class BriefingTileComponent extends Component
{
  /** @var string */
  public $position;

  public $cols;

  public function mount(string $position)
  {
    $this->position = $position;
  }

  public function render()
  {
    return view('dashboard-briefing-tile::tile', [
      'briefings' => BriefingStore::make()->briefings(),
      'refreshIntervalInSeconds' =>
        config('dashboard.tiles.briefing.refresh_interval_in_seconds') ?? 15,
    ]);
  }
}
