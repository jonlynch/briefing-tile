<?php

namespace JonLynch\BriefingTile\Services;

use Illuminate\Support\Facades\Http;
//use Carbon\Carbon;

class Briefings
{
  public static function getBriefings(string $url): array
  {
    return Http::get($url)->json();
  }
}
