<?php

namespace JonLynch\BriefingTile;

use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use JonLynch\BriefingTile\Commands\FetchBriefingDataCommand;

class BriefingTileServiceProvider extends ServiceProvider
{
  public function boot()
  {
    if ($this->app->runningInConsole()) {
      $this->commands([FetchBriefingDataCommand::class]);
    }

    $this->publishes(
      [
        __DIR__ . '/../resources/views' => resource_path(
          'views/vendor/dashboard-briefing-tile'
        ),
      ],
      'dashboard-briefing-tile-views'
    );

    $this->loadViewsFrom(
      __DIR__ . '/../resources/views',
      'dashboard-briefing-tile'
    );

    Livewire::component('briefing-tile', BriefingTileComponent::class);
  }
}
