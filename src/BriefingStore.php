<?php

namespace JonLynch\BriefingTile;

use Spatie\Dashboard\Models\Tile;

class BriefingStore
{
  private Tile $tile;

  public static function make()
  {
    return new static();
  }

  public function __construct()
  {
    $this->tile = Tile::firstOrCreateForName("Briefing");
  }

  public function setBriefings(array $data): self
  {
    $this->tile->putData('Briefing', $data);

    return $this;
  }

  public function briefings(): array
  {
    return $this->tile->getData('Briefing') ?? [];
  }
}
