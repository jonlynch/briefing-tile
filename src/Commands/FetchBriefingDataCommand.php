<?php

namespace JonLynch\BriefingTile\Commands;

use Illuminate\Console\Command;
use JonLynch\BriefingTile\BriefingStore;
use JonLynch\BriefingTile\Services\Briefings;

class FetchBriefingDataCommand extends Command
{
  protected $signature = 'dashboard:fetch-briefing-data';

  protected $description = 'Fetch Briefing data';

  public function handle()
  {
    $this->info('Fetching Briefing data');

    $briefings = Briefings::getBriefings(
      config('dashboard.tiles.briefing.url')
    );

    BriefingStore::make()->setBriefings($briefings);

    $this->info('All done!');
  }
}
